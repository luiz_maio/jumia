<?php

use App\controller\ClientController;

/**
 * ClientControllerTest
 **/
class ClientControllerTest extends PHPUnit\Framework\TestCase
{
    public function testGetClientsFiltersMethod()
    {
        $validCountriesCodes = array('', '212', '237', '251', '256', '258');
        $validState = array('', '0', '1');
        $this->assertContains('212', $validCountriesCodes);
        $this->assertContains('0', $validState);
    }

    public function testGetDataPhoneMethod()
    {
        $client = new ClientController();
        $phone = '(111) 123456';

        $this->assertIsArray($client->getDataPhone($phone));
    }

    public function testGetCountryByCodeMethod()
    {
        $client = new ClientController();
        
        $invalidCode = '111';

        // Morocco
        $code = '212';
        $this->assertEquals('Morocco', $client->getCountryByCode($code));

        // Cameroon
        $code = '237';
        $invalidCode = '111';
        $this->assertEquals('Cameroon', $client->getCountryByCode($code));

        // Ethiopia
        $code = '251';
        $invalidCode = '111';
        $this->assertEquals('Ethiopia', $client->getCountryByCode($code));

        // Uganda
        $code = '256';
        $invalidCode = '111';
        $this->assertEquals('Uganda', $client->getCountryByCode($code));

        // Mozambique
        $code = '258';
        $invalidCode = '111';
        $this->assertEquals('Mozambique', $client->getCountryByCode($code));

        $this->assertEquals('Not Found', $client->getCountryByCode($invalidCode));
    }

    public function testValidatePhoneMethod()
    {
        $client = new ClientController();

        // Morocco
        $code = '212';
        $invalidPhone = '(212) 123456';
        $validPhone = '(212) 698054317';
        $this->assertEquals('Not Valid', $client->validatePhone($code, $invalidPhone));
        $this->assertEquals('Valid', $client->validatePhone($code, $validPhone));

        // Cameroon
        $code = '237';
        $invalidPhone = '(237) 123456';
        $validPhone = '(237) 697151594';
        $this->assertEquals('Not Valid', $client->validatePhone($code, $invalidPhone));
        $this->assertEquals('Valid', $client->validatePhone($code, $validPhone));

        // Ethiopia
        $code = '251';
        $invalidPhone = '(251) 123456';
        $validPhone = '(251) 911168450';
        $this->assertEquals('Not Valid', $client->validatePhone($code, $invalidPhone));
        $this->assertEquals('Valid', $client->validatePhone($code, $validPhone));

        // Uganda
        $code = '256';
        $invalidPhone = '(256) 123456';
        $validPhone = '(256) 704244430';
        $this->assertEquals('Not Valid', $client->validatePhone($code, $invalidPhone));
        $this->assertEquals('Valid', $client->validatePhone($code, $validPhone));

        // Mozambique
        $code = '258';
        $invalidPhone = '(258) 123456';
        $validPhone = '(258) 847602609';
        $this->assertEquals('Not Valid', $client->validatePhone($code, $invalidPhone));
        $this->assertEquals('Valid', $client->validatePhone($code, $validPhone));
    }
}