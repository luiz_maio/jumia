<?php    
    require_once __DIR__ . '/../../vendor/autoload.php';

    use App\controller\ClientController;

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $filters = false;

    if(!empty($_GET));
        $filters = $_GET;

    $dataClients = new ClientController;  
    $clients = $dataClients->getClients($filters);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Jumia Test</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
    <h1>Phone Numbers</h1>
    <div class="filters">
        <form action="index.php" method="get">
            <select name="country">
                <option value="">Choose any Country</option>
                <option value="237">Cameroon</option>
                <option value="251">Ethiopia</option>
                <option value="212">Morocco</option>
                <option value="258">Mozambique</option>
                <option value="256">Uganda</option>
            </select>
            <select name="state">
                <option value="">Choose any State</option>
                <option value="1">Valid</option>
                <option value="0">Not Valid</option>
            </select>
            <input type="submit" value="Search">
        </form>
    </div>
    <div class="table">
        <table>
            <thead>
                <td>Name</td>
                <td>Country</td>
                <td>State</td>
                <td>Country Code</td>
                <td>Number</td>                
            </thead>
            <?php
                if($clients){
                    foreach ($clients as $client) {
                        echo '<tr>';
                        echo '<td>' . $client['name'] . '</td>';
                        echo '<td>' . $client['country'] . '</td>';
                        echo '<td>' . $client['state'] . '</td>';
                        echo '<td>' . $client['code'] . '</td>';
                        echo '<td>' . $client['phone'] . '</td>';
                        echo '</tr>';
                    }
                }
            ?>
        </table> 
    </div>
</body>
</html>