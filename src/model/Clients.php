<?php
namespace App\model;

// require_once '../model/Connection.php';

use App\model\Connection;

class Clients
{
    /**
     * Connection variable.
     *
     * @var Connection
     */
    private $connection;

    public function __construct()
    {
        $conn = new Connection;
        $this->connection = $conn->getdbconnect('../model/sample.db');
    }

    /**
     * getClients Controller
     *
     * @return array  $clients
     */
    public function getClients()
    {
        $clients = array();
        $db = $this->connection;
        $tablesquery = $db->query("SELECT `name`, `phone` FROM customer;");
        while ($table = $tablesquery->fetchObject()) {
            $clients[] = array(
                'name' => $table->name,
                'phone' => $table->phone
            );
        }
        return $clients;
    }
}
