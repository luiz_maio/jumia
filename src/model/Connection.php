<?php

namespace App\model;

class Connection
{
    public function getdbconnect($model)
    {
        $conn = new \PDO("sqlite:" . $model);
        if(!$conn){
            return 'Fail Connection';
        }
        return $conn;
    }
}