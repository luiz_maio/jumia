<?php
namespace App\controller;

use App\model\Clients;


class ClientController
{
    /**
     * getClients Controller
     *
     * @param  array  $filters
     * @return array  $clients
     */
    public function getClients($filters = array())
    {
        $clients = array();
        $clientsModel = new Clients;
        $allClients = $clientsModel->getClients();

        foreach ($allClients as $client) {
            $phone = $this->getDataPhone($client['phone']);
            $tempClient = array(
                'name' => $client['name'],
                'country' => $phone['country'],
                'state' => $phone['state'],
                'code' => $phone['code'],
                'phone' => trim($phone['number'])
            );
            if(empty($filters)){
                $clients[] = $tempClient;
            } else {
                if($filters['country'] == "" && $filters['state'] == ""){
                    $clients[] = $tempClient;
                }
                if($filters['country'] != "" && $filters['state'] == ""){
                    if($filters['country'] == $phone['code'])
                        $clients[] = $tempClient;
                }
                if($filters['country'] == "" && $filters['state'] != ""){
                    if($filters['state'] == "0" && $phone['state'] == 'Not Valid')
                        $clients[] = $tempClient;
                    if($filters['state'] == "1" && $phone['state'] == 'Valid')
                        $clients[] = $tempClient;
                }
                if($filters['country'] != "" && $filters['state'] != ""){
                    if($filters['country'] == $phone['code'] && $filters['state'] == "0" && $phone['state'] == 'Not Valid')
                        $clients[] = $tempClient;
                    if($filters['country'] == $phone['code'] && $filters['state'] == "1" && $phone['state'] == 'Valid')
                        $clients[] = $tempClient;
                }
            }
        }
        return $clients;
    }

    /**
     * getDataPhone method.
     *
     * @param  string  $phone
     * @return array  $data
     */
    public function getDataPhone($phone)
    {
        $code = substr($phone, 1, 3);
        $data = array(
            'code' => $code,
            'number' => substr($phone, 5),
            'country' => $this->getCountryByCode($code),
            'state' => $this->validatePhone($code, $phone)
        );
        return $data;        
    }

    /**
     * getCountryByCode method.
     *
     * @param  string  $code
     * @return string  country
     */
    public function getCountryByCode($code)
    {
        switch ($code) {
            case '237':
                return 'Cameroon';
            case '251':
                return 'Ethiopia';
            case '212':
                return 'Morocco';
            case '258':
                return 'Mozambique';
            case '256':
                return 'Uganda';
            default:
                return 'Not Found';
        }
    }

    /**
     * validatePhone method.
     *
     * @param  string  $code
     * @param  string  $phone
     * @return string  $state
     */
    public function validatePhone($code, $phone)
    {
        $pm = false;
        $state = 'Not Valid';
        switch ($code) {
            case '237':
                $pm = preg_match('"\(237\)\ ?[2368]\d{7,8}$"', $phone);
                break;
            case '251':
                $pm = preg_match('"\(251\)\ ?[1-59]\d{8}$"', $phone);
                break;
            case '212':
                $pm = preg_match('"\(212\)\ ?[5-9]\d{8}$"', $phone);
                break;
            case '258':
                $pm = preg_match('"\(258\)\ ?[28]\d{7,8}$"', $phone);
                break;
            case '256':
                $pm = preg_match('"\(256\)\ ?\d{9}$"', $phone);
                break;
            default:
                $pm = false;
                break;
        }
        $state = $pm ? 'Valid' : 'Not Valid';
        return $state;
    }
}